#!/bin/bash
# Convert avi's to mp4 or ogg
# DavidTangye@gmail.com 02/07/2010

# Todo:
#	- Process audio? (or use Audacity?)
#	- Add other mpegs: See http://en.wikipedia.org/wiki/MPEG & http://wiki.videolan.org/MPEG
#	- Add modules/filters
#		Logo? --sub-filter=logo logo-file=logo1.png,3000;logo2.png,8000 video.mpg
#		Subtitles? :sub-file=file1.srt
#			see http://wiki.videolan.org/Subtitles & http://wiki.videolan.org/SubtitlesCodec
#			MPSub, SubRip, SubViewer
#		Time?
#		Metadata: Author, etc
#	- Create front end using zenity to feed the parameters to this script?

vProg="`basename $0`"
showHelp() {
	cat <<EOF
${vProg} [parameters as per below] video-filenames ...

	-v|--verbose:	gives verbose output
	-h|--help:		shows this help

	-s|--scale:			scale the size of the video, default = 1 (same as input)
	-c|--channels:		number of audio channels, default = 1

	--ogg|--m4v|--mp3:	output encapsulation, ogg, mpeg-4 video, or mpeg1.3 audio
		 ogg; codecs: video=theo, audio=vorb (default)
		 mp4; codecs: video=mp4v, audio=mpga
EOF
}

if [ "${1}." == "." ] ; then showHelp ; exit ; fi

# Default mux/container/encapsulation
vMux=ogg
vCodec=theo
vAcodec=vorb
vExt=ogv
# and its vlc parameters
vVb=800
vAb=32

# Defaults that work for all muxes/containers/encapsulations :-)
vFps=15
vScale=1
vChannels=1

vLog="/tmp/${vProg}-$$.log"

GetExt () {
	if [ "`echo -n "$1" | grep '\.'`" ];then
		vE="$1"
		while [ "`echo -n "$vE" | grep '\.'`" ];do
			vE="`echo -n "$vE" | cut -f2- -d\.`"
		done
		echo "$vE"
	fi
}

#for p in "$@" ; do
for p ; do
	if [ "`echo $p|cut -c1`" == "-" ] ; then
		case "$p" in
			-v|--verbose)
				vErbose=".";;
			-h|--help)
				vHelp=".";;
			-s|--scale)
				vScale="$p";;
			-c|--channels)
				vChannels="$p";;
			--m4v)
				vExt=m4v		# Official standard is mp4 - use either
				vMux=mp4
				vAcodec=mpga	# Recommended over mp4a for wider use
				vAb=40
				vCodec=mp4v
				vVb=1100;;		# Range of 800(breakups) - 1200?
			--mp3)
				vExt=mp3		# Audio only. BUGGED: The output does not play.
				vMux=mpeg1
				vAcodec=mpga;;
		esac
	fi
done

if [ "$vHelp" ] ; then showHelp ; exit ; fi

#for p in "$@" ; do
for p ; do
	if [ "`echo $p|cut -c1`" != "-" ] && [ -f "$p" ] ; then
		vDir="`dirname "$p"`"
		vInBase="`basename "$p"`"
		vInExt="`GetExt "$vInBase"`"
		vOut="`echo ${vInBase}|sed -e "s/\.${vInExt}$//" -`.${vExt}"
		if [ "$vExt" == "mp3" ] ; then
			vVlcParms="#transcode{acodec=${vAcodec}}:std{access=file,mux=${vMux},dst=${vDir}/${vOut}}"
		else
			vSub="`echo ${vInBase}|sed -e "s/\.${vInExt}$//" -`.srt"
			if [ -f "${vDir}/${vSub}" ] ; then vSubParm=",soverlay" ; else vSubParm="" ; fi # ?? :subtitle{sub-type{subrip}}
		vVlcParms="#transcode{vcodec=${vCodec},fps=${vFps},vb=${vVb},scale=${vScale},acodec=${vAcodec},ab=${vAb},channels=${vChannels}${vSubParm}}:std{access=file,mux=${vMux},dst=${vDir}/${vOut}}"
		fi

		vFiles=`echo ${vFiles:=0} + 1|bc`
		if [ "$vFiles" == "1" ] ; then
			if [ "$vExt" == "mp3" ] ; then
				vMsg="$vProg processing __\n\tconverting files to ${vMux} - .${vExt} (Codecs: audio=${vAcodec})\n\t--sout ${vVlcParms}"
			else
				vMsg="$vProg processing __\n\tconverting files to ${vMux} - .${vExt} (Codecs: video=${vCodec}, audio=${vAcodec})\n\t--sout ${vVlcParms}"
			fi
			if [ "$vErbose" ] ; then
				echo -e "$vMsg" | tee "${vLog}"
			else
				echo -e "$vMsg"
			fi
		fi
		if [ "$vErbose" ] ; then
			echo -e "\n\t- ${p} to\n\t  ${vOut} ..." | tee -a "${vLog}"
			cvlc --sout "${vVlcParms}" "$p" vlc://quit >>"${vLog}" 2>&1
		else
			cvlc --sout "${vVlcParms}" "$p" vlc://quit
		fi
	fi
done
if [ "$vErbose" ] ; then
	echo -e "\n\t$vFiles conversions logged in ${vLog}" | tee -a "${vLog}"
fi

